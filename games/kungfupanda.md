---
title: "Kung Fu Panda: Showdown of Legendary Legends"
img: /games/img/kungfupanda.png
type:
  - Full Game
tags:
  - Casual
  - Licensed
  - 3D
platforms:
  - PS4
  - PS3
  - Xbox 360
  - Xbox One
  - 3DS
  - Wii U
  - PC
maxplayers:
  - 4
rostersize:
  - 24
video: https://www.youtube-nocookie.com/embed/QHiCxJ4IqoA
longdescription:
  - 'Kung Fu Panda: Showdown of Legendary Legends is a fighting video game based on the Kung Fu Panda franchise. It features voice acting from both the Film and TV series. As of 2019, the game is no longer available in digital form.'
---
