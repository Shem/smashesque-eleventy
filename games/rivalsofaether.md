---
title: "Rivals of Aether"
img: /games/img/rivalsofaether.png
type:
  - Full Game
tags:
  - Indie
  - Cameos
  - Competitive
  - Technical
  - Pixel Art
platforms:
  - PC
  - Xbox One
maxplayers:
  - 4
rostersize:
  - 14
video: https://www.youtube-nocookie.com/embed/5JHnBjzVKXg
longdescription:
  - 'Rivals of Aether is a fighting game created by Dan Fornace. The characters are animal champions from different warring civilizations, who battle to stop a shadowy force from threatening the world. Other characters are also available as downloadable content, such as Ori and Sein from Ori and the Blind Forest. A focus in the development of the game was making it faster and more combo-oriented than Super Smash Bros.'
---
