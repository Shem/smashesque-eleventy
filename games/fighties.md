---
title: "Fighties"
img: /games/img/fighties.png
type:
  - Full Game
tags:
  - Pixel Art
  - Casual
  - Indie
platforms:
  - PC
maxplayers:
  - 5
rostersize:
  - 32
video: https://www.youtube-nocookie.com/embed/vMwACeoLHQA
longdescription:
  - Fighties is a fast paced 2D platform fighter featuring randomly generated and user-created levels.
---
