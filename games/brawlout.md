---
title: "Brawlout"
img: /games/img/brawlout.png
type:
  - Full Game
tags:
  - Indie
  - Cameos
  - Technical
  - '3D'
platforms:
  - PC
  - Switch
  - PS4
maxplayers:
  - 4
rostersize:
  - 25
video: https://www.youtube-nocookie.com/embed/APtwdOD-T8o
longdescription:
  - Brawlout is a fighting game developed and published by Angry Mob Games. It includes guest fighters from Hyper Light Drifter, Guacamelee!, Yooka-Laylee, and Dead Cells. Interestingly, Brawlout does not use blocking, and most characters are incapable of grabbing others. Instead, its gameplay is based on combos.
---
