---
title: "Samurai Gunn"
img: /games/img/samuraigunn.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Technical
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/a4Gwmxol8GY
longdescription:
  - 'Samurai Gunn is a local multiplayer game utilising melee and shooting mechanics as well as platforming. Players are armed with a sword and gun with only three bullets per life. A match typically consists of players defeating each other with one hit with an attack with either their sword or a bullet. Both swords and bullets can be deflected by other players with precise timing.'
---
