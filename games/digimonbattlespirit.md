---
title: "Digimon Battle Spirit"
img: /games/img/dbs.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - Retro
  - Pixel Art
platforms:
  - GBA
maxplayers:
  - 2
rostersize:
  - 12
video: https://www.youtube-nocookie.com/embed/qjUPSqjCoHQ
longdescription:
  - Digimon Battle Spirit (デジモンテイマーズ　バトルスピリット) features characters and Digimon that were included in the first three seasons of the animated series of the same name in a somewhat simplistic fighting scenario, and also has slightly arranged samples of the show's soundtrack.
---
