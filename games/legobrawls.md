---
title: "LEGO Brawls"
img: /games/img/legobrawls.jpg
type:
  - Full Game
tags:
  - Casual
  - Licensed
  - 3D
  - Custom Characters
platforms:
  - Mobile
maxplayers:
  - 8
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/rOt-sVtbyNI
longdescription:
  - 'Lego Brawls is multiplayer online brawler for Apple Arcade featuring custom characters and a variety of weapons & abilities.'
---
