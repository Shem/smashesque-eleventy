---
title: "Rumble Arena"
img: /games/img/rumblearena.png
type:
  - Full Game
tags:
  - 3D
  - Casual
  - F2P
  - Multiplayer Only
platforms:
  - PC
  - Mobile
maxplayers:
  - 4
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/mhxIYyBCoFA
longdescription:
  - 'Rumble Arena is a free crossover brawling game featuring legends and gods from history and the galaxy. It is one of the few Smashesque games available on Mobile.'
---
