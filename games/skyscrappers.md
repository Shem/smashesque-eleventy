---
title: "SkyScrappers"
img: /games/img/skyscrappers.png
type:
  - Full Game
tags:
  - Indie
  - Story Mode
  - Casual
platforms:
  - PC
  - Switch
maxplayers:
  - 4
rostersize:
  - 4
video: https://www.youtube-nocookie.com/embed/dF_MN-cqYL0
longdescription:
  - 'SkyScrappers is a classic arcade-style, vertically scrolling, competitive platform-fighting game. Players fight and race their way to the top of a falling skyscraper as it is being demolished.'
---
