---
title: "Duck Game"
img: /games/img/duckgame.png
type:
  - Full Game
tags:
  - Indie
  - Casual
  - Pixel Art
  - Custom Characters
platforms:
  - PC
  - PS4
  - Switch
  - Ouya
maxplayers:
  - 4
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/HXbFaRS4x34
longdescription:
  - Duck Game is a 2D action video game that features shooting and platforming mechanics. The game has a simple premise, one hit and you are dead, last player standing wins the round, play to some target number of rounds. The game has several unique elements including multiple hats, a quack button, and a ragdoll button.
---
