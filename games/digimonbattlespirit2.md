---
title: "Digimon Battle Spirit 2"
img: /games/img/dbs2.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - Retro
  - Pixel Art
platforms:
  - GBA
maxplayers:
  - 2
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/hyr9ar3czbk
longdescription:
  - Digimon Battle Spirit 2 (バトルスピリット デジモンフロンティア) is a versus battle game, where one of the Human Spirit Digimon fights the other. Unlike most fighting games, the winner is not determined by having the most health, but the most blue or red "D-Spirits", which are released whenever the player strikes his/her opponent.
---
