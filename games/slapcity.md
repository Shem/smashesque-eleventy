---
title: "Slap City"
img: /games/img/slapcity.png
type:
  - Full Game
tags:
  - Indie
  - Casual
  - 3D
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/qk9P_WY9izk
longdescription:
  - 'Slap City is a streamlined platform fighter with characters and locations from the Ludosity universe. The game includes a unique, team-based Slap Ball mode.'
---
