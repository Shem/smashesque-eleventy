---
title: "Dream Mix TV World Fighters"
img: /games/img/dmtvwf.png
type:
  - Full Game
tags:
  - Crossover
  - Casual
  - 3D
  - Licensed
platforms:
  - PS2
  - GameCube
maxplayers:
  - 4
rostersize:
  - 17
video: https://www.youtube-nocookie.com/embed/qrAGvWCrSPI
longdescription:
  - 'DreamMix TV World Fighters (Japanese: ドリームミックスTV ワールドファイターズ Hepburn: Dorīmumikkusu Tībī Wārudo Faitāzu) is a crossover fighting video game developed by Bitstep and published by Hudson for the Nintendo GameCube and PlayStation 2 in Japan on December 18, 2003. The game crosses over characters from Hudson, Konami and Takara including Solid Snake from Metal Gear Solid and Optimus Prime from The Transformers.'
---
