---
title: "Shovel Knight Showdown"
img: /games/img/shovelknightshowdown.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Spinoff
  - Casual
platforms:
  - PC
  - PS3
  - PS4
  - Xbox One
  - Switch
  - Wii U
maxplayers:
  - 4
rostersize:
  - 16
video: https://www.youtube-nocookie.com/embed/Nr81mmA_inA
longdescription:
  - 'Shovel Knight Showdown contains duels with up to 4 players who scramble after gems as their favorite heroic or villainous knight. Many Shovel Knight characters are playable, some even for the first time ever.'
---
