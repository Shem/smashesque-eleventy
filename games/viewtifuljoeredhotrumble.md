---
title: "Viewtiful Joe: Red Hot Rumble"
img: /games/img/viewtifuljoeredhotrumble.png
type:
  - Full Game
tags:
  - Story Mode
  - Casual
  - 3D
  - Spinoff
platforms:
  - GameCube
  - PS2
  - PSP
maxplayers:
  - 4
rostersize:
  - 12
video: https://www.youtube-nocookie.com/embed/w-kZuI8URww
longdescription:
  - 'Viewtiful Joe: Red Hot Rumble, known in Japan as Viewtiful Joe: Battle Carnival, is a video game for the Nintendo GameCube and the PlayStation Portable. It is a slightly mission-based fighting/beat em up that uses a modified engine from previous games in the series. It featuring characters and powers from the Viewtiful Joe games and anime.'
---
