---
title: "Black & White Bushido"
img: /games/img/bwbushido.png
type:
  - Full Game
tags:
  - Casual
  - Indie
  - Multiplayer Only
platforms:
  - PC
  - PS4
  - Switch
maxplayers:
  - 4
rostersize:
  - 4
video: https://www.youtube-nocookie.com/embed/dR8fONEnJhU
longdescription:
  - Black & White Bushido is a two-tone, Japanese-themed fighter in which opposing sides use stealthy tactics to hide in areas that are the same colour as their characters.
---
