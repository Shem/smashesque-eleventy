---
title: "Outfoxies, The"
img: /games/img/outfoxies.png
type:
  - Full Game
tags:
  - Retro
  - Guns
  - Pixel Art
platforms:
  - Arcade
maxplayers:
  - 1
rostersize:
  - 7
video: https://www.youtube.com/embed/HLD3__O36Eg
longdescription:
  - 'The Outfoxies is a fighting game developed and published by Namco for Japanese arcades in 1995. It is a one-on-one fighting game set on large stages that change dynamically over the course of a battle. Players must use weapons to attack the other player while also avoiding the dangers and havoc brought on by various stage gimmicks. Although the game did not attract much interest at release, it has gained recognition in retrospect for the variety and dynamic elements in its game design, and for its outlandish and bizarre action themes. The Outfoxies is also recognized for pioneering gameplay elements seen in later arena fighting games, particularly the Super Smash Bros. series.'
---
