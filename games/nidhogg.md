---
title: "Nidhogg"
img: /games/img/nidhogg.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Casual
  - Arguable
platforms:
  - PC
  - PS4
  - PS Vita
maxplayers:
  - 2
rostersize:
  - N/A
video: https://www.youtube.com/embed/4dxtAfnXixs
longdescription:
  - 'Nidhogg is a fast-paced two-player duelling game where two players sword fight in a side-scrolling environment. Players can run, jump, slide, throw their swords, and fistfight. The player-characters sword can be held at three different heights: low, medium, and high, and changing the swords position to hit the opponents sword will disarm the opponent.'
---
