---
title: "Superfighters Deluxe"
img: /games/img/superfightersdeluxe.png
type:
  - Full Game
tags:
  - Pixel Art
  - Guns
  - Casual
platforms:
  - PC
maxplayers:
  - 8
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/iwQcTxTXYNE
longdescription:
  - 'Superfighters Deluxe is an action game that combines brawling, shooting and platforming in dynamic sandbox levels. Lots of weapons and gameplay systems interlock to create action-movie-like chaos.'
---
