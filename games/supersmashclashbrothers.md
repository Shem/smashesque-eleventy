---
title: "Super Smash Clash Brothers"
img: /games/img/sscb.jpg
type:
  - Full Game
tags:
  - Pixel Art
  - Indie
  - Casual
platforms:
  - Mobile
maxplayers:
  - 4
rostersize:
  - 6
video: https://www.youtube-nocookie.com/embed/0KC7anuRtCA
longdescription:
  - 'Super Smash Clash - Brothers (a.k.a. Super Brawl Smash) is a mobile-exclusive platform fighter for Android phones'
---
