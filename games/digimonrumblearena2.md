---
title: "Digimon Rumble Arena 2"
img: /games/img/dra2.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - 3D
  - Retro
platforms:
  - GameCube
  - PS2
  - Xbox
maxplayers:
  - 4
rostersize:
  - 19
video: https://www.youtube-nocookie.com/embed/8IQmKcTKSXw
longdescription:
  - 'Digimon Rumble Arena 2 (デジモンバトルクロニクル) is based on the Digivolving techniques of Digimon battling it out in a battle royale and to see who is the strongest Digimon. The game features characters from the first four seasons of the anime: Digimon Adventure, Digimon Adventure 02, Digimon Tamers, and Digimon Frontier. Each one has their own special moves and "digivolutions", as well as slightly varying normal attacks and taunts.'
---
