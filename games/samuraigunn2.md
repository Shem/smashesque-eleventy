---
title: "Samurai Gunn 2"
img: /games/img/samuraigunn2.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Technical
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - N/A
video: https://www.youtube-nocookie.com/embed/a4Gwmxol8GY
longdescription:
  - 'Samurai Gunn is back, with a new, expansive adventure mode and an updated versus mode. Adventure with a friend through a medieval metropolis on the brink of crisis—fight or fly past your foes to discover the mystery behind a ghostly threat.'
---
