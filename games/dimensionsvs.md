---
title: "DimensionsVS"
img: /games/img/dvs.png
type:
  - Full Game
tags:
  - 3D
  - Casual
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 6
video: https://www.youtube-nocookie.com/embed/BOvaOnYaQ7U
longdescription:
  - DimensionsVS is a chaotic, action-packed platform fighting game supporting both online and local play, and is completely free to play. DimensionsVS is under constant development, with new stages, characters and game-modes being added with every major update.
---
