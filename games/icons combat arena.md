---
title: "Icons: Combat Arena"
img: /games/img/icons.png
type:
  - Full Game
tags:
  - Indie
  - Technical
  - 3D
  - Competitive
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/kJ_9iXBIGOw
longdescription:
  - 'Icons: Combat Arena is a fast-paced platform fighting game offering players tight controls, powerful special attacks. It was developed by the team who previously created the famous Project M mod for Super Smash Brothers Brawl. The game is unfortunately no longer available.'
---
