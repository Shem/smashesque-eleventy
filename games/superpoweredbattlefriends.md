---
title: "Super Powered Battle Friends"
img: /games/img/spbf.png
type:
  - Full Game
tags:
  - Pixel Art
  - Indie
  - Casual
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 4
video: https://www.youtube-nocookie.com/embed/WtkLKVes_3E
longdescription:
  - 'Super Powered Battle Friends is a 2D indie platform fighter featuring hand crafted pixel art and a colourful cast of characters. The game features custom colours for each character and is built on Unreal Engine 4.'
---
