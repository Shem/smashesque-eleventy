---
title: "Mystic Melee"
img: /games/img/mysticmelee.png
type:
  - Game Mode
tags:
  - Indie
  - Pixel Art
  - Casual
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 4
video: https://www.youtube-nocookie.com/embed/KgLAtEbo3KA
longdescription:
  - 'Mystic Melee brings players a mix of physics-based platforming and spellcasting. The game features a four-player battle mode.'
---
