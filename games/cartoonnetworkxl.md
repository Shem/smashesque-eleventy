---
title: "Cartoon Network Punch Time Explosion XL"
img: /games/img/cnptexl.png
type:
  - Full Game
tags:
  - Crossover
  - Casual
  - Licensed
  - '3D'
platforms:
  - Wii
  - PS3
  - Xbox 360
maxplayers:
  - 4
rostersize:
  - 26
video: https://www.youtube-nocookie.com/embed/Znk5EX3LgK4
longdescription:
  - 'Cartoon Network: Punch Time Explosion XL is an upgraded port of the 3DS game. It adds eight new playable characters to the roster, seven new assist characters, five new stages, and additional gameplay modes. In addition, it features updated voice acting for some characters.'
---
