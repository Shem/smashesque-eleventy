---
title: "Small Arms"
img: /games/img/smallarms.png
type:
  - Full Game
tags:
  - Guns
  - Casual
  - 3D
platforms:
  - Xbox 360
maxplayers:
  - 4
rostersize:
  - 14
video: https://www.youtube-nocookie.com/embed/QloQ6e-1xFE
longdescription:
  - 'Small Arms is a hybrid platform, fighting and shoot em up video game, featuring anthropomorphic animals and super-deformed people, with ranged weapons. Similar to Geometry Wars, the player controls character movement with the left analog stick, and aims weapons with the right analog stick, which allows 360-degree aiming. Each weapon has primary and secondary modes, which the player can use with the right and left triggers, respectively. The player can also make characters jump or dash with the face buttons.'
---
