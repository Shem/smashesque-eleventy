---
title: "Jump! Super Stars"
img: /games/img/jumpss.png
type:
  - Full Game
tags:
  - Crossover
  - Anime
  - Casual
  - Pixel Art
platforms:
  - DS
maxplayers:
  - 4
rostersize:
  - 34
video: https://www.youtube-nocookie.com/embed/g-cqt5LR7Z0
longdescription:
  - 'Jump Super Stars is a 2D crossover fighting game for the Nintendo DS, based on Weekly Shōnen Jump characters. Koma (panel) is the term for the characters that the player can use in the game. There are three types of koma: help koma, support koma and battle koma. Komas can be collected and organized to unlock bonuses, characters and skills.'
---
