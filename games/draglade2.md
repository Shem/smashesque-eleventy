---
title: "Draglade 2"
img: /games/img/draglade2.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - Retro
  - Pixel Art
  - Arguable
platforms:
  - DS
maxplayers:
  - 1
rostersize:
  - 10
video: https://www.youtube-nocookie.com/embed/I-bU1IHxH9c
longdescription:
  - Draglade 2 introduces the "Beat Drive" system, which is similar to that of a tug-of-war underneath the opponent lifebars. When your segment reaches the opponent's side, you'll get either enhanced stats or a temporary-machine super.
---
