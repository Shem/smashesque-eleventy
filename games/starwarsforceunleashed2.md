---
title: "Star Wars: The Force Unleashed 2 (Multiplayer)"
img: /games/img/starwars.png
type:
  - Game Mode
tags:
  - Casual
  - 3D
  - Arguable
  - Multiplayer Only
  - Licensed
platforms:
  - Wii
maxplayers:
  - 4
rostersize:
  - 8
video: https://www.youtube-nocookie.com/embed/v1TwXvSdB4k
longdescription:
  - 'Star Wars: The Force Unleashed 2 on Wii contains an exclusive multiplayer battle mode in which players can use various characters and abilities from the Star Wars universe to do battle.'
---
