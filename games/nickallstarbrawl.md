---
title: "Nickelodeon All-Star Brawl"
img: /games/img/nickasb.jpg
type:
  - Full Game
tags:
  - Crossover
  - Cartoon
  - Technical
  - '3D'
platforms:
  - PS4
  - PS5
  - Switch
  - Xbox
  - PC
maxplayers:
  - 4
rostersize:
  - 20
video: https://www.youtube.com/watch?v=BIuGYhVDAis
longdescription:
  - Nickelodeon All-Star Brawl is a crossover fighting game developed by Ludosity and Fair Play Labs and published by GameMill Entertainment. It is the first console game in the long-running Nickelodeon Super Brawl series of browser games and mobile games, featuring characters from various Nickelodeon shows.
---
