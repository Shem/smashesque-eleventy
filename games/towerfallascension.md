---
title: "Towerfall: Ascension"
img: /games/img/towerfallascension.png
type:
  - Full Game
tags:
  - Indie
  - Projectile
  - Competitive
  - Pixel Art
  - Technical
platforms:
  - PS4
  - Xbox One
  - Switch
  - PC
  - Ouya
maxplayers:
  - 4
rostersize:
  - 18
video: https://www.youtube-nocookie.com/embed/hVGHCs82EqQ
longdescription:
  - 'TowerFall is an action indie video game created by Matt Thorson through their company Matt Makes Games. In the game, players control up to four archers in a battle royale. TowerFall is an archery combat arena game where players kill each other with arrows and head-stomps until only one player remains.'
---
