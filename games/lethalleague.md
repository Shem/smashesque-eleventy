---
title: "Lethal League"
img: /games/img/lethalleague.png
type:
  - Full Game
tags:
  - Sports
  - Technical
  - Competitive
  - Arguable
platforms:
  - PS4
  - Xbox One
  - PC
maxplayers:
  - 2
rostersize:
  - 7
video: https://www.youtube-nocookie.com/embed/DO8GaQekLkA
longdescription:
  - 'Lethal League is a 2D arena fighting video game. The goal is to hit a ball back and forth, and have the ball hit other players until there is only one player left. With each consecutive hit, the ball will speed up more, making it harder for the other players to not get hit and hit the ball back.'
---
