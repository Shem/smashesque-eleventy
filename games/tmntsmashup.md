---
title: "Teenage Mutant Ninja Turtles: Smash Up"
img: /games/img/tmntsmashup.png
type:
  - Full Game
tags:
  - Story mode
  - Licensed
  - 3D
  - Casual
platforms:
  - Wii
  - PS2
maxplayers:
  - 4
rostersize:
  - 16
video: https://www.youtube-nocookie.com/embed/91Mt_0bGsvA
longdescription:
  - 'Teenage Mutant Ninja Turtles: Smash-Up is a 2.5D fighting game game developed by Game Arts in cooperation with Mirage Studios. The game stresses interaction with the environment, and stages in the game feature traps, changes to the stage itself and interactive elements.'
---
