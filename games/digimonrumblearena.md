---
title: "Digimon Rumble Arena"
img: /games/img/dra.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - 3D
  - Retro
platforms:
  - PS1
maxplayers:
  - 2
rostersize:
  - 24
video: https://www.youtube-nocookie.com/embed/07Gzkxl0hrg
longdescription:
  - Digimon Rumble Arena (デジモンテイマーズ バトルエボリューション) is a crossover fighting video game developed and published by Bandai in association with Hudson Soft. Players are able to choose from up to 24 characters (fifteen of which need to be unlocked) from the Digimon Adventure and Digimon Tamers seasons of the Digimon anime, such as Terriermon, Agumon, Gatomon, and Renamon.
---
