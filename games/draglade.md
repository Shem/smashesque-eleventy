---
title: "Draglade"
img: /games/img/draglade.png
type:
  - Full Game
tags:
  - Anime
  - Casual
  - Retro
  - Pixel Art
  - Arguable
platforms:
  - DS
maxplayers:
  - 1
rostersize:
  - 10
video: https://www.youtube-nocookie.com/embed/VB8F0VLF1Po
longdescription:
  - Draglade (カスタムビートバトル ドラグレイド) is a fighting video game with music video game and role-playing game elements. The fighting system is different from other fighting games in that there are not a lot of directional inputs needed for moves. Instead, special moves are set by collecting "Bullets" and then activating them with the DS's touch screen.
---
