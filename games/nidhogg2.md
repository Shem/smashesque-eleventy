---
title: "Nidhogg 2"
img: /games/img/nidhogg2.png
type:
  - Full Game
tags:
  - Indie
  - Pixel Art
  - Casual
  - Arguable
platforms:
  - PC
  - PS4
  - Switch
maxplayers:
  - 4
rostersize:
  - N/A
video: https://www.youtube.com/embed/mYWtSi8JqjI
longdescription:
  - 'Nidhogg 2 is a fighting game in which two player duel against each other. The player has to reach the end of their opponents side first wins. Players have a variety of moves, including sliding and leaping. They can deflect their opponents attacks. Weapons, such as daggers, throwing knives and bows, as well as a character creator, are introduced.'
---
