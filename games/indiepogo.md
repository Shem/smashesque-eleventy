---
title: "Indie Pogo"
img: /games/img/indiepogo.png
type:
  - Full Game
tags:
  - Crossover
  - Indie
  - Casual
  - Pixel Art
platforms:
  - PC
maxplayers:
  - 4
rostersize:
  - 16
video: https://www.youtube-nocookie.com/embed/ZLIfREs8qzg
longdescription:
  - 'Indie Pogo is an indie crossover fighting video game developed and published by Lowe Bros. Studios. The game features characters and settings from more than 50 different indie games, such as Shovel Knight, VVVVVV, Teslagrad, the Bit.Trip series, and Freedom Planet. Unlike other fighting games, the primary gameplay mechanic of Indie Pogo is auto-jumping: characters landing on the ground or a platform will automatically jump back into the air.'
---
