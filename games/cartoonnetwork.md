---
title: "Cartoon Network Punch Time Explosion"
img: /games/img/cnpte.png
type:
  - Full Game
tags:
  - Crossover
  - Casual
  - Licensed
  - '3D'
platforms:
  - 3DS
maxplayers:
  - 4
rostersize:
  - 18
video: https://www.youtube-nocookie.com/embed/oA3bGxD1MRY
longdescription:
  - 'Cartoon Network: Punch Time Explosion features characters from various Cartoon Network animated series battling against one another.'
---
