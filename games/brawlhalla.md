---
title: "Brawlhalla"
img: /games/img/brawlhalla.png
type:
  - Full Game
tags:
  - Indie
  - Cameos
  - Technical
  - '2D'
platforms:
  - PC
  - PS4
maxplayers:
  - 4
rostersize:
  - 21
video: https://www.youtube-nocookie.com/embed/ZD_ymaFKjC4
longdescription:
  - Brawlhalla is a free-to-play fighting game. Every week, Brawlhalla has a rotation of 8 selectable characters for newer players to use for free. It features various cameos from franchises such as Hellboy, Shovel Knight and Adventure Time.
---
