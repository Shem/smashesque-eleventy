const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const {
  DateTime
} = require("luxon");
const CleanCSS = require("clean-css");
const pluginPWA = require("eleventy-plugin-pwa");


module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(eleventyNavigationPlugin);
  eleventyConfig.addPassthroughCopy("games/img");
  eleventyConfig.addPassthroughCopy("css");
  eleventyConfig.addPassthroughCopy("manifest.json");

  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj, {
      zone: 'utc'
    }).toFormat("dd-MM-yy");
  });

  // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
  eleventyConfig.addFilter('htmlDateString', (dateObj) => {
    return DateTime.fromJSDate(dateObj, {
      zone: 'utc'
    }).toFormat('yy-MM-dd');
  });

  // Get the first `n` elements of a collection.
  eleventyConfig.addFilter("head", (array, n) => {
    if (n < 0) {
      return array.slice(n);
    }

    return array.slice(0, n);
  });

  eleventyConfig.setBrowserSyncConfig({
    // scripts in body conflict with Turbolinks
    snippetOptions: {
      rule: {
        match: /<\/head>/i,
        fn: function(snippet, match) {
          return snippet + match;
        }
      }
    }
  });

  eleventyConfig.addPlugin(pluginPWA, {
    swDest: "./build/service-worker.js",
    globDirectory: "./build",
    clientsClaim: true,
    skipWaiting: true
  });

  // Create "restaurants in platform" collections keyed by platform name
  eleventyConfig.addCollection("platformCollections", function(collection) {
    let resultArrays = {};
    collection.getAll().forEach(function(item) {
       if(item.data["title"] && item.data["platforms"]) {
         if( !resultArrays[item.data["platforms"]] ) {
           resultArrays[item.data["platforms"]] = [];
         }      resultArrays[item.data["platforms"]].push(item);
       }  });  return resultArrays;});

  eleventyConfig.setDataDeepMerge(true);
  eleventyConfig.addLayoutAlias("game", "layouts/game.njk");
  eleventyConfig.addLayoutAlias('footer', 'layouts/footer.njk');
  eleventyConfig.addCollection("tagList", require("./_11ty/getTagList"));
  eleventyConfig.addCollection("platformList", require("./_11ty/getPlatformList"));
  eleventyConfig.addPassthroughCopy("manifest.json");
  eleventyConfig.addPassthroughCopy("192.png");
  eleventyConfig.addPassthroughCopy("512.png");
  eleventyConfig.addPassthroughCopy("apple-touch-icon.png");
  eleventyConfig.addPassthroughCopy("favicon.png");
  eleventyConfig.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  })

};
